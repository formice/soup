#maybe module
module MaybeModule
  def maybe
    @isUseMaybe = 1
    return self unless self.nil?
    o = Object.new
      class << o 
        def method_missing(sym, *args) 
          "#{sym}" == "just" ? nil : nil.maybe
        end
      end
    o
  end
end

#object include Module
class Object  
include MaybeModule  
end 

class Soup 
  def initialize(content)
    @content = content
  end
  
  def isNil(str)
    if str == "" || str == nil || str == "nil"
      return true
    else
      return false
    end
  end
  
  def head
     if isNil(@content) && @isUseMaybe == 1
      @temp = nil.maybe
      return nil.maybe
    else
      last = @content.index("</html>")-"</html>".length+1;
      @temp = @content["<html>".length,last]
      return  self
    end
  end
  
  def title
    if isNil(@temp) && @isUseMaybe == 1
      @temp = nil.maybe
      return nil.maybe
    else
      last = @temp.index("</head>")-"</head>".length+1;
      @temp = @temp["<head>".length,last]
      return  self
    end
    
  end
  
  def string
    if isNil(@temp) && @isUseMaybe == 1
      @temp = nil.maybe
      return nil.maybe
    else
      last = @temp.index("</title>")-"</title>".length+1;
      @temp = @temp["<title>".length,last]
      return self
    end
    
  end
  
  def just
    return @temp
  end
  
  def to_s
    return @temp
  end
end

soup = nil
#puts soup.head.title.string -->NoMethodError
puts soup.maybe.head.title.string.just

soup = Soup.new("<html><head><title>Hello World!</title></head></html>")
puts soup.maybe.head.title.string.just

soup = Soup.new("<html></html>")
puts soup.maybe.head.title.string.just

soup = Soup.new(nil)
puts soup.maybe.head.title.string.just

soup = Soup.new("<html></html>")
#puts soup.head.title.string.just

soup = Soup.new(nil)
#puts soup.head.title.string.just


soup = Soup.new("<html><head><title>Hello World!</title></head></html>")
puts soup.maybe.head.just

soup = Soup.new("<html><head><title>Hello World!</title></head></html>")
puts soup.head
